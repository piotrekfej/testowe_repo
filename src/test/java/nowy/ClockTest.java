package nowy;

import homework3.Clock;
import homework3.exceptions.HoursOutOfBoundException;
import homework3.exceptions.MinutesOutOfBoundException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClockTest {
    private Clock clock = new Clock();

    private void checkClock(int hours, int minutes){
        assertEquals(hours, clock.getHours());
        assertEquals(minutes, clock.getMinutes());
    }

    private void checkClockWithSeconds(int hours, int minutes, int seconds){
        assertEquals(hours, clock.getHours());
        assertEquals(minutes, clock.getMinutes());
        assertEquals(seconds, clock.getSeconds());
    }

   //1
    @Test
    public void create_clock_with_starting_time(){
    checkClock(0,0);
    }

    //2
    @Test(expected = HoursOutOfBoundException.class)
    public void set_hours_to_minus() throws Exception{
        clock.setHours(-4);
        assertEquals(4, clock.getHours());
    }

    //3
    @Test
    public void set_hours_higher_then_23() throws Exception{
        clock.setHours(27);
        assertEquals(3, clock.getHours());
    }

    //4
    @Test(expected = MinutesOutOfBoundException.class)
    public void set_minutes_to_minus() throws Exception{
        clock.setMinutes(-3);
        assertEquals(3, clock.getMinutes());
    }

    //5
    @Test
    public void set_minutes_up_to_59()throws Exception{
        clock.setMinutes(63);
        assertEquals(3, clock.getMinutes());
    }

    //6 Dodanie minut bez przekroczenia pełnej godziny (np. 10:30 + 29 minut)
    @Test
    public void add_minutes_without_exceed_hour() throws Exception{
        clock.addMinutes(40);
        checkClock(0,40);
    }

    //7 Dodanie minut z przekroczeniem godziny (np. 10:30 + 49 minut).
    @Test
    public void add_minutes_to_exceed_hour()throws Exception{
        clock.addMinutes(71);
        checkClock(1,11);
    }

    //8 Dodanie minut z przekroczeniem godziny i doby (np. 21:22 + 300 minut).
    @Test
    public void add_minutes_to_exceed_hour_and_day()throws Exception{
        clock.setHours(21);
        clock.setMinutes(22);
        clock.addMinutes(300);
        checkClock(2,22);
    }

    //9 Dodanie ujemnej liczby minut.
    @Test
    public void add_minus_minutes() throws Exception{
        clock.addMinutes(-15);
        checkClock(0,15);
    }

    //10
    @Test
    public void add_clock_with_zero_time(){
        Clock clock2 = new Clock();
        clock.addNewClock(clock2);
        checkClock(0,0);
    }
    //12
    @Test
    public void add_clock_to_exceed_hours()throws Exception{
        clock.setHours(10);
        clock.setMinutes(49);
        Clock clock2 = new Clock(0, 30);
        clock.addNewClock(clock2);
        checkClock(11,19);
    }

    //13
    @Test
    public void add_clock_to_exceed_hours_and_day() throws Exception{
        clock.setHours(22);
        clock.setMinutes(22);
        Clock clock2 = new Clock(0, 300);
        clock.addNewClock(clock2);
        checkClock(3,22);
    }

    //14
    @Test
    public void add_clock_without_exceed_hours() throws Exception{
        clock.setHours(10);
        clock.setMinutes(30);
        Clock clock2 = new Clock(0,29);
        clock.addNewClock(clock2);
        checkClock(10,59);
    }

    @Test
    public void add_clock_with_minus_minutes() throws Exception{
        Clock clock2 = new Clock(0,-7);
        clock.addNewClock(clock2);
        checkClock(0,7);
    }

    @Test
    public void create_clock_with_seconds_accurancy(){
        checkClockWithSeconds(0,0,0);
    }


}
